<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

use Monolog\Logger;

class Foo2
{

    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function bar($var = null)
    {

        if (null === $var) {

            // Log unexpected behaviour
            $this->logger->log(Logger::ERROR, '$var needs to be set', [$var,'File: ' =>  __FILE__, 'Line: ' => __LINE__]);

            // Logged by default with Whoops/Monolog if uncaught
            throw new \InvalidArgumentException('$var needs to be set');
        }

        return ['foo', 'bar'];
    }
}

try {

    $foo = new Foo2($logger);
    $arrFoo = $foo->bar();
    if (in_array('foo', $arrFoo)) {
        echo 'Foo<br />';
    }
    print_r($arrFoo);

} catch (\InvalidArgumentException $e) {
    echo 'Argument Exception caught: ' . $e->getMessage();
} catch (\Exception $e) {
    echo 'General Exception caught: ' . $e->getMessage();
}