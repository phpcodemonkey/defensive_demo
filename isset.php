<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

$foo = $_REQUEST;

// Bad example
if (isset($foo['myfield'])) {

    echo 'myfield 1: ' . $foo['myfield'] . '<br />';
}


// Bad example - better than above, but still weak as not checking type
if (array_key_exists('myfield', $foo) && $foo['myfield'] == true) {

    echo 'myfield 2: ' . $foo['myfield'] . '<br />';
}

// Defensive example - Test value and type as well as presence
$foo = filter_input_array(INPUT_GET, ['myfield' => FILTER_VALIDATE_BOOLEAN]);

if (array_key_exists('myfield', $foo) && $foo['myfield'] === true) {

    echo 'myfield 3: ' . $foo['myfield'] . '<br />';
}

print_r($_REQUEST);