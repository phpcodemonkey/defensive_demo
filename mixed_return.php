<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

class Foo
{

    public function bar($var = null)
    {

        if (is_null($var)) { // Old style is_null
            return false; // Early return of different type than expected
        }

        return ['foo', 'bar'];
    }
}

$foo = new Foo();
$arrFoo = $foo->bar();
if (in_array('foo', $arrFoo)) {
    echo 'Foo<br />';
}
print_r($arrFoo);
