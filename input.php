<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';


$email = $_REQUEST['email'];

// Bad example - Email regexps are notoriously difficult to understand and maintain!
if (preg_match('/^[^@]+@([^@\.]+\.)+[^@\.]+$/', $email)) {
    echo 'Email valid<br />';
} else {
    echo 'Email invalid<br />';
}


// Defensive example
$email = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_EMAIL); // Remove all characters except letters, digits and !#$%&'*+-=?^_`{|}~@.[]

if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // RFC 822 with the exceptions that comments and whitespace folding are not supported.
    echo 'Defensive: Email valid<br />';
} else {
    echo 'Defensive: Email invalid<br />';
}