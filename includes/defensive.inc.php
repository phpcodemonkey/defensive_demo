<?php

include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use Dotenv\Dotenv;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SlackHandler;
use Whoops\Run;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\JsonResponseHandler;


// Load the .env file
if (is_readable(__DIR__ . DIRECTORY_SEPARATOR . '.env')) {
    $dotenv = new Dotenv(__DIR__);
    $dotenv->load();
}


// Set up monolog
$logger = new Logger('demo');

if (is_readable(__DIR__ . DIRECTORY_SEPARATOR . 'demo.log')) {
    $logger->pushHandler(
    // Set up the log file
        new StreamHandler(
            __DIR__ . DIRECTORY_SEPARATOR . 'demo.log',
            Logger::DEBUG
        )
    );
}

if (array_key_exists('SLACK_API_TOKEN', $_ENV) && array_key_exists('SLACK_CHANNEL', $_ENV)) {
    $logger->pushHandler(
    // Set up the slack log handler
        new SlackHandler(
            $_ENV['SLACK_API_TOKEN'],// Loaded from .env file
            $_ENV['SLACK_CHANNEL'],// Provide your own channel or private channel
            'Monolog',
            true,
            null,
            Logger::DEBUG,// Change the log level to vary chattiness of output
            true,
            true,
            true
        )
    );
    $_ENV['SLACK_API_TOKEN'] = $_SERVER['SLACK_API_TOKEN'] = '*** Protected ***';
    putenv('SLACK_API_TOKEN="*** Protected ***"');
}


// Set up Whoops
$whoops = new Run();

// Add the default Whoops handlers
$whoops->pushHandler(new JsonResponseHandler);
$whoops->pushHandler(new PrettyPageHandler);

// Set up the monolog custom handler
$whoops->pushHandler(
    function ($exception, $inspector, $run) use ($logger) {

        /** @var $exception \Exception */
        $logger->log(
            Logger::ERROR,
            $logger->getName() . ': ' . $exception->getMessage(),
            [
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ]
        );
    }
);

$whoops->register();