<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

use Monolog\Logger;

$foo = 123;

// Bad example - no default section
switch ($foo) {
    case 345:
        echo '$foo: 345';
        break;

    case 456:
        echo '$foo: 456';
        break;

    case 567:
        echo '$foo: 567';
        break;
}


// Defensive example
switch ($foo) {
    case 345:
        echo '$foo: 345';
        break;

    case 456:
        echo '$foo: 456';
        break;

    case 567:
        echo '$foo: 567';
        break;

    default:
        // Log unexpected behaviour
        $logger->log(Logger::ERROR, '$foo - unexpected value', [$foo, 'File' => __FILE__, 'Line: ' => __LINE__]);
        break;
}