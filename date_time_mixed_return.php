<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

$contract = [ 'start_date' => null, 'end_date' => date('Y-m-d') ]; // start_date result from DB was null

// Bad example - Real world example discovered (via NewRelic) during talk preparation
$startDate = DateTime::createFromFormat('Y-m-d', $contract['start_date']);
$endDate = DateTime::createFromFormat('Y-m-d', $contract['end_date']);

$dateRange = $startDate->format('d/m/Y'); // assuming that $startDate is an instance of DateTime class
$dateRange .= ' to ' . $endDate->format('d/m/Y'); // assuming that $startDate is an instance of DateTime class

echo $dateRange . '<br />';


// Defensive example
$dateRange = 'Unknown start/end';

//$contract['start_date'] = date('Y-m-d', strtotime('yesterday'));

if (null === $contract['start_date']) {
    $logger->log(\Monolog\Logger::WARNING, 'Contract start date is null', [$contract, 'File: ' . __FILE__ , 'Line: ' . __LINE__]);
} else {

    $startDate = DateTime::createFromFormat('Y-m-d', $contract['start_date']);
    if ($startDate) { // DateTime::createFromFormat returns false on failure - mixed return type!
        $dateRange = $startDate->format('d/m/Y');
    }

    if (null === $contract['end_date']) {
        $logger->log(\Monolog\Logger::WARNING, 'Contract end date is null', [$contract, 'File: ' . __FILE__, 'Line: ' => __LINE__]);
    } else {
        $endDate = DateTime::createFromFormat('Y-m-d', $contract['end_date']);
        if ($endDate) {
            $dateRange .= ' to ' . $endDate->format('d/m/Y');
        }
    }
}

echo $dateRange . '<br />';