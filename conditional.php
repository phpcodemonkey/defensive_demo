<?php

include_once __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'defensive.inc.php';

use Monolog\Logger;

$foo = 123;

// Bad example - no else block
if ($foo == 345) {
    echo '$foo: 345';
} elseif ($foo == 456) {
    echo '$foo: 456';
} elseif ($foo == 567) {
    echo '$foo: 567';
}


// Defensive example
if (345 === $foo) {
    echo '$foo: 345';
} elseif (456 === $foo) {
    echo '$foo: 456';
} elseif (567 === $foo) {
    echo '$foo: 567';
} else {
    // Log unexpected behaviour
    $logger->log(Logger::ERROR, '$foo - unexpected value', [$foo, 'File: ' => __FILE__, 'Line: ' => __LINE__]);
}