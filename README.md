# README #

### This repository is for ###

* Demo files for The Lost Art of Defensive Programming talk ([Related slides](http://bit.ly/LostArtDefensiveProgramming))

### How do I get set up? ###

This is a simple set up requiring a low number of composer dependencies and the creation of a couple of files for   
logging. Optionally you can also use a [Slack](https://slack.com) integration with an API token.

#### Configuration ####

###### Log file
Create a writeable demo.log file in the includes folder. On OS X this would be similar to:

```
#!bash

touch includes/demo.log
sudo chgrp _www includes/demo.log
sudo chmod g+w includes/demo.log
```
(substitute `_www` group for `apache` or `www-data` etc. as required on Linux)

###### Environment variables file
Create a .env file in includes folder
Add your [Slack API Token](https://api.slack.com/web) to includes/.env file


    SLACK_API_TOKEN="xxxxxxx"


#### Dependencies ####

Install required dependencies through [composer](https://getcomposer.org/)

```
#!bash

php composer.phar install
```

### Contribution guidelines ###

* See the talk, read the slides, play with examples - contributions welcome though as these code snippets are for
illustrative purposes, just feel free to apply the principles in your work practices.

### Who do I talk to? ###

* [@phpcodemoney](https://twitter.com/phpcodemonkey) or via [Bitbucket](http://bit.ly/ArtDefensiveDemo)